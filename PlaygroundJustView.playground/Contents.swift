import Cocoa

var str = "Hello, playground"

func makeSentence(_ input:[Substring]) throws -> String  {
  let r: String
  let length = input.count

  switch length {
  case 0: throw MyError.shit("zero length")
  case 1: r = "only \(input[0])"
  case 2: r = "only \(input[0]) or \(input[1])"
  default: r = input[1..<length-1]
    .reduce("only \(input[0])") {a, b in
      a + ", " + b} + " or \(input[length-1])"
  }
  return r
}

let a = "those that this".split(separator: " ")
do {
  try print(makeSentence([]))
} catch MyError.shit(let s) {
  print("SHITE \(s)")
}
