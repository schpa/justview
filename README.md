# TL;DR

A stay on top window you can drag images into and which updates the contents when it detects changes. The app remembers the last filename but not the contents, it also remembers the window position.

# What

I could not find any viewer that updated its content if the source had changed, which was odd. I wrote an utility that just gives you a window you can drop images into and the window will update on changes.

When reopened, it attempts to restore last position and content, but if the file is moved, you will get an error and an empty window.

# Why

I was enjoying Clojure, producing images using [rhizome.viz](https://github.com/ztellman/rhizome) — playing around with graphs in various forms and shapes — so when I detected bodily stress, you know moving from one app to the other to update content, back and forth, I created this. A tiny thing that would always show me the most updated version of a produced image.

Working example:

![doc/snapshot1.png](doc/snapshot1.png)

# How

You probably need pod and xcode. 

Clone the repo, install [https://cocoapods.org](https://cocoapods.org/), do pod install for the source-root, and open the workspace-file graciously created for you by Pod.

Compile/run your binary and drop an image into it.

# Where

Download it here [binary](doc/JustView.zip)
