//
//  ViewController.swift
//  JustView
//
//  Created by Chris Patrick Schreiner on 13/05/2019.
//  Copyright © 2019 schpa. All rights reserved.
//

// todo: avoid sending Defaults.set while resizing, only set it at the release

import Cocoa
import EonilFileSystemEvents
import SwiftyUserDefaults

let debugging:Bool = false //1 == Int(ProcessInfo.processInfo.environment["TEST_MODE"]) ?? 0

enum MyError: Error {
  case shit(String)
}

func storeDefaultPosition(_ position: NSRect) {
  Defaults.set(NSStringFromRect(position), forKey: "rect")
}

func storeDefaultFilename(_ filename: String) {
  Defaults.set(filename,forKey: "filename")
}

func trackFilename(_ path: String, view: DropView) -> FileSystemEventMonitor? {
  if debugging { print("start trackFilename ", path) }
  storeDefaultFilename(path)

  return FileSystemEventMonitor(
    pathsToWatch: [
      path
    ],
    latency: 1,
    watchRoot: false,
    queue: .main) { (events: [FileSystemEvent])->() in
      view.maybeRestoreImage()
  }
}

func makeSentence(_ input:[Substring]) throws -> String  {
  let r: String
  let length = input.count

  switch length {
  case 0: throw MyError.shit("zero length!")
  case 1: r = "\(input[0])"
  case 2: r = "\(input[0]) or \(input[1])"
  default: r = input[1..<length-1]
    .reduce("\(input[0])") {a, b in
      a + ", " + b} + " or \(input[length-1])"
  }
  return r
}

class ViewController: NSViewController {
  @IBOutlet var dropView: DropView!

  override func viewDidLoad() {
    super.viewDidLoad()
    view.window?.level = .floating
    view.wantsLayer = true
    view.layerContentsRedrawPolicy = .onSetNeedsDisplay
//    Defaults.removeAll()
  }

  override func viewWillAppear() {
    if let path = Defaults.string(forKey: "filename"),
      let rect = Defaults.string(forKey: "rect") {
      self.view.window?.setFrame(NSRectFromString(rect), display: true)
      dropView.tracker = trackFilename(path, view: dropView)
    }
  }
}


//
//
//
//

class DropView: NSView, NSWindowDelegate {
  @IBOutlet weak var applicationMessage: NSTextField!

  var tracker: FileSystemEventMonitor?
  var filePath: String?
  let expectedExt:[Substring] = "png gif jpeg jpg".split(separator: " ")
  let fm = FileManager.default

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    registerForDraggedTypes([NSPasteboard.PasteboardType.URL, NSPasteboard.PasteboardType.fileURL])
  }

  func windowDidMove(_ notification: Notification) {
    storeDefaultPosition((notification.object as? NSWindow)?.frame ?? NSRect.zero)
  }

  func windowWillResize(_ sender: NSWindow, to frameSize: NSSize) -> NSSize {
    storeDefaultPosition(NSRect(origin: sender.frame.origin, size: frameSize))
    return frameSize
  }

  override func viewDidMoveToWindow() {
    self.window!.level = .floating
    self.window!.delegate = self
    maybeRestoreImage()
  }

  func setApplicationMessage(msg: String) {
    applicationMessage?.isHidden = false
    applicationMessage?.stringValue = msg
  }

  func hideApplicationMessage() {
    applicationMessage?.isHidden = true
  }

  fileprivate func checkExtension(_ drag: NSDraggingInfo) -> Bool {
    guard let board =
      drag.draggingPasteboard.propertyList(forType: NSPasteboard.PasteboardType(rawValue: "NSFilenamesPboardType")) as? NSArray,
      let path = board[0] as? String
      else { return false }

    let suffix = URL(fileURLWithPath: path).pathExtension
    for ext in self.expectedExt {
      if ext.lowercased() == suffix {
        layer?.backgroundColor = NSColor.systemGreen.cgColor
        clearImage()
        setApplicationMessage(msg: "Sure, I'll take it")
        return true
      }
    }

    layer?.backgroundColor = NSColor.systemRed.cgColor
    clearImage()
    try? setApplicationMessage(msg: "Nope — thats not a \(makeSentence(expectedExt.map{".\($0)"}))"  )
    return false
  }

  func setImage(_ path: String) {
    if let layer = self.layer {
      layer.contentsGravity = .resizeAspect
      layer.contents = NSImage(byReferencingFile: path)
      layer.borderColor = NSColor.unemphasizedSelectedContentBackgroundColor.cgColor
    }
  }

  func clearImage() {
    if let layer = self.layer {
      layer.contents = nil
      layer.backgroundColor = NSColor.unemphasizedSelectedContentBackgroundColor.cgColor
    }
  }

  func maybeRestoreImage() {
    if debugging { print("maybeRestoreImage") }
    hideApplicationMessage()
    clearImage()

    if let path = self.filePath {
      if fm.fileExists(atPath: path) {
        setImage(path)
      } else {

        setApplicationMessage(msg: "File not found, \(path)")
      }
    } else {
      if let path = Defaults.string(forKey: "filename") {
        if fm.fileExists(atPath: path) {
          setImage(path)
          return
        }
      }

      // todo remove the try/catch
      do {
        if let x = try .some(makeSentence(expectedExt.map{".\($0)"})) {
          setApplicationMessage(msg: "Drop a \(x) here")
        }
      } catch MyError.shit(let x) {
        setApplicationMessage(msg: "? \(x)")
      } catch {
        setApplicationMessage(msg: "xxx")
      }
    }
  }

  override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
    if checkExtension(sender) == true {
      return .copy
    } else {
      return NSDragOperation()
    }
  }

  override func draggingExited(_ sender: NSDraggingInfo?) {
    maybeRestoreImage()
  }

  override func draggingEnded(_ sender: NSDraggingInfo) {
    maybeRestoreImage()
  }

  override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
    guard let pasteboard = sender.draggingPasteboard.propertyList(forType: NSPasteboard.PasteboardType(rawValue: "NSFilenamesPboardType")) as? NSArray,
      let path = pasteboard[0] as? String
      else { return false }

    setImage(path)
    tracker = trackFilename(path, view: self)
    self.filePath = path

    return true
  }
}
