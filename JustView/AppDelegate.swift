//
//  AppDelegate.swift
//  JustView
//
//  Created by Chris Patrick Schreiner on 13/05/2019.
//  Copyright © 2019 schpa. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

  func applicationDidFinishLaunching(_ aNotification: Notification) {
    // Insert code here to initialize your application
  }

  func applicationWillTerminate(_ aNotification: Notification) {
    // Insert code here to tear down your application
  }


}

